const express = require('express');
const path = require('path');
// const sls = require('serverless-http');
const connectDB = require('./config/db');

const app = express();

connectDB();

// init middleware
app.use(express.json({ extended: false }));

// Define routes
app.use('/api/users', require('./routes/users'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/profile', require('./routes/profile'));
app.use('/api/info', require('./routes/info'));

if (process.env.NODE_ENV === 'production') {
  // static folder
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on ${PORT}`));

// module.exports.server = sls(app);
