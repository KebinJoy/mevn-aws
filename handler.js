'use strict';

const mongoose = require('mongoose');
const config = require('config');

const db = config.get('mongoURI');

let cachedConnection = null;

const connectDB = async () => {
  if (cachedConnection && cachedConnection.readyState == 1) {
    console.log('=> using cached db');
    return Promise.resolve(cachedConnection);
  }

  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log('MongoDB connected...');
    cachedConnection = mongoose.connection;
    return Promise.resolve(cachedConnection);
  } catch (err) {
    return Promise.reject(err.message);
    // process.exit(1);
  }
};

exports.helloWorld = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectDB()
    .then((connection) => {
      console.log('Sucessfully connected boo yeah!');
      const response = {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        },
        body: JSON.stringify({
          message: 'Go Serverless v1.0! Your function executed successfully!',
          input: event,
        }),
      };

      callback(null, response);
    })
    .catch((err) => {
      const errRes = {
        statusCode: 400,
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
          message: err,
          input: event,
        }),
      };

      callback(null, errRes);
    });
};
