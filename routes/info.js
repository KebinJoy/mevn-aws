const express = require('express');

const router = express.Router();

// @route   GET api/info
// @desc    Get app info
// @access  Public
router.get('/', (req, res) => {
  res.send({ application: 'Sample app', version: '1.0' });
});

module.exports = router;
